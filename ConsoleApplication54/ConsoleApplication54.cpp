﻿#include <iostream>
#include <ctime>
using namespace std;
const int SIZE = 5;

void Set(int array[][SIZE])
{
    for (int i = 0; i < SIZE; i++)
        for (int j = 0; j < SIZE; j++)
            array[i][j] = rand() % 100;
}

void PRINT(int array[][SIZE])
{
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
            cout << array[i][j] << '\t';
        cout << endl;
    }
}

int LineSearch(int array[][SIZE], int key)
{
    int index = 0;
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            index++;
            if (array[i][j] == key)
                return index;
        }
    }
    return -1;
}

void main()
{
    srand((int)time(0));
    int array[SIZE][SIZE], key;
    Set(array);
    PRINT(array);
    cout << "ISKOMIY ELEMENT ";
    cin >> key;

    if (LineSearch(array, key) == -1)
        cout << "\n NET elementa" << endl;
    else cout << "\n ELEMENT NAYDEN, NOMER ELEMENTA : " << LineSearch(array, key) + 1 << endl;
}